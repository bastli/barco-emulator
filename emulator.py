import time
import socket
import gi
import argparse
import numpy as np
from gi.repository import GLib

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from gi.repository import Gdk

gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf

parser = argparse.ArgumentParser(description='Emulate the Barco Strips')
parser.add_argument('--ip', type=str, default="0.0.0.0", help="IP to bind to")
parser.add_argument('--port', type=int, default=1337, help="Port to bind to")
parser.add_argument('--cairo', action="store_const", default=False, const=True, help="Use cairo to draw (slow)")
parser.add_argument('--loose-aspect', dest="aspect", action="store_false", help="Do not Keep the aspect ratio")

args = parser.parse_args()

UDP_IP = args.ip
UDP_PORT = args.port

WITH_PIXBUF = not args.cairo

# https://stackoverflow.com/questions/18160315/write-custom-widget-with-gtk3
class BarcoWidget(Gtk.Misc):
	__gtype_name__ = 'BarcoWidget'
	
	def __init__(self, array, radius=2, *args, **kwds):
		super().__init__(*args, **kwds)
		self.array = array;
		self.radius = radius;
	
	def do_draw(self, cr):
		# paint background
		cr.set_source_rgba(0,0,0,1)
		cr.paint()
		# draw a diagonal line
		allocation = self.get_allocation()
		fg_color = self.get_style_context().get_color(Gtk.StateFlags.NORMAL)
		
		if WITH_PIXBUF:
			pb = pixbuf_from_array(self.array)
			
			if args.aspect:
				wr = allocation.width/pb.get_width()
				hr = allocation.height/pb.get_height()
				r = min(wr,hr)
				w = int(pb.get_width()*r)
				h = int(pb.get_height()*r)
			else:
				w = max(allocation.width, pb.get_width())
				h = max(allocation.height, pb.get_height())
			   
			pb = pb.scale_simple(w, h, GdkPixbuf.InterpType.NEAREST)
			Gdk.cairo_set_source_pixbuf (cr,
				pb,
				allocation.width/2 - w/2, 
				allocation.height/2 - h/2);
			cr.paint()
		else:
			h,w,c = self.array.shape;
			dh = max(1, allocation.height/h)
			dw = max(1, allocation.width/w)
			for y,d in enumerate(self.array):
				y = y*dh
				for x,c in enumerate(d):
					x = x*dw
					c=c/255.0
					cr.set_source_rgb(c[0],c[1],c[2]);
					cr.arc(x,y, self.radius, 0, 2*np.pi);
					cr.fill()
	
	def update(self, array):
		self.array = array

win = Gtk.Window()
win.set_title("Barco Emulator")
win.connect('destroy', Gtk.main_quit)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
print('LISTENING ON %{}:%{}'.format(UDP_IP, UDP_PORT))

strips = np.ones((112,15,3),dtype=np.uint8)*255;

def pixbuf_from_array(z):
	" convert from numpy array to GdkPixbuf "
	" From https://stackoverflow.com/questions/39936737/how-to-turn-gdk-pixbuf-object-into-numpy-array#41714464"
	z = z.astype('uint8')
	h,w,c = z.shape
	return GdkPixbuf.Pixbuf.new_from_data(z.tobytes(),  GdkPixbuf.Colorspace.RGB, c==4, 8, w, h, w*c, None, None)

img = BarcoWidget(strips);

img.set_hexpand(True);
img.set_vexpand(True);

win.add(img);

def trigger_redraw():
	ts = time.time();
	img.queue_draw()

max_fs = 60;
ts = time.time()
def new_data(*args, **kwargs):
	global ts
	data,remote = sock.recvfrom(1024)
	if time.time()-ts > 1.0/max_fs:
		addr = int(data[0])
		data = data[1:]
		if (addr >=0) and (addr < 15):
			strips[:,addr,:].flat = list(map(np.int8,data))
			trigger_redraw();
	return True

socksrc = GLib.IOChannel.unix_new(sock.fileno())
socksrc.add_watch(GLib.IOCondition.IN, new_data)

win.show_all()
Gtk.main()

sock.close()
